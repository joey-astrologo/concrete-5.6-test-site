<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

class AddPlayerController extends Controller {
	
		public function view() {
			Loader::Model('PlayerModel');
			$players = Player::getPlayers();
			$this->set('players',$players);
		}
		public function add() {
			$uName = $this->post('uName');
			Loader::Model('PlayerModel');
			$this->set('added', Player::addPlayer($uName));
			$this->view();
		}
		public function delete($pId, $pName) {
			Loader::Model('PlayerModel');
			$this->set('deleted', Player::deletePlayer($pId, $pName));
			$this->view();
		}
}
?>
