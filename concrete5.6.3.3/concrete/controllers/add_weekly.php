<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

class AddWeeklyController extends Controller {
	public function view() {
		Loader::Model('WeeklyModel');
		$tickets = Weekly::getWeeklies();
		$this->set('tickets',$tickets);
	}
	public function addWeeklyTickets() {
		$numbers = $this->post('numbers');
		Loader::Model('WeeklyModel');
		$success = Weekly::insertWeeklyTicket($numbers);
		$this->set('success',$success);
		$this->view();
	}
	public function deleteWeeklyTickets($tId) {
		
		Loader::Model('WeeklyModel');
		$ticket = Weekly::deleteWeeklyTicket($tId);

		$this->set('WeeklyId', $ticket);
		$this->view();
	}
	public function reset() {
		Loader::Model('WeeklyModel');
		$ticket = Weekly::fullCheckMatches();
		$this->view();
	}
}
?>