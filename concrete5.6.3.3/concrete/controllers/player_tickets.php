<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

class PlayerTicketsController extends Controller {
	public function on_before_render() {
		Loader::Model('PlayerModel');
		$players = Player::getPlayers();
		$this->set('players',$players);
	}
	public function addTickets() {
		$pId = $this->post('sPlayer');
		$numbers = $this->post('numbers');
		
		Loader::Model('TicketModel');
		$success = Ticket::insertTickets($pId,$numbers);
		
		Loader::Model('PlayerModel');
		$pName = Player::getPlayer($pId);
		
		$this->set('nums', $numbers);
		$this->set('pName', $pName[0]['name']);
		$this->set('result', $success);
		$this->on_before_render();
	}
}
?>