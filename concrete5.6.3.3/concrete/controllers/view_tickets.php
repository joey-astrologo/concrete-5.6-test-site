<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

class ViewTicketsController extends Controller {
	public function on_before_render() {
		Loader::Model('PlayerModel');
		$players = Player::getPlayers();
		$this->set('players',$players);
	}
	public function getPlayerTickets() {
		$pId = $this->post('sPlayer');
		
		Loader::Model('TicketModel');
		$tickets = Ticket::getTickets($pId);
		
		Loader::Model('PlayerModel');
		$pName = Player::getPlayer($pId);
		
		$this->set('tickets', $tickets);
		$this->set('pName', $pName[0]['name']);

		$this->on_before_render();
	}
	public function deletePlayerTickets($tId) {
		
		Loader::Model('TicketModel');
		$ticket = Ticket::deleteTicket($tId);

		$this->set('ticketId', $ticket);
		$this->on_before_render();
	}
}
?>