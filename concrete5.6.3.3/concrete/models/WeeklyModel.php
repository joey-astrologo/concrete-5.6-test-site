<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

class Weekly {
	public static function myRange($value) {
		$min = 1;
		$max = 49;
		if(($min <= $value) && ($value <= $max)) {
			return true;
		}
		else {
			return false;
		}
	}
	public static function getWeeklies() {
		$db = Loader::db( 'localhost', 'root', 'root', 'game', true);
		$sql='select id, n1, n2, n3, n4, n5, n6 from weeklyTickets order by id ASC;';
		$result = $db->Execute($sql);
		$db = Loader::db(null, null, null, null, true);
		if(($result) && (!$result->EOF)) {
			while ($row = $result->FetchRow()) {
				$tickets[]= $row;
			}
			return $tickets;
		} else {
			return false;
		}
	}
	public static function getWeekliesNoId() {
		$db = Loader::db( 'localhost', 'root', 'root', 'game', true);
		$sql='select n1, n2, n3, n4, n5, n6 from weeklyTickets order by id ASC;';
		$result = $db->Execute($sql);
		$db = Loader::db(null, null, null, null, true);
		if(($result) && (!$result->EOF)) {
			while ($row = $result->FetchRow()) {
				$tickets[]= $row;
			}
			return $tickets;
		} else {
			return false;
		}
	}
	public static function insertWeeklyTicket($ticket) {
	
		$db = Loader::db( 'localhost', 'root', 'root', 'game', true);
		
		$n1 = $ticket[0];
		$n2 = $ticket[1];
		$n3 = $ticket[2];
		$n4 = $ticket[3];
		$n5 = $ticket[4];
		$n6 = $ticket[5];
		if(Weekly::myRange($n1) && Weekly::myRange($n2) && Weekly::myRange($n3) && Weekly::myRange($n4) && Weekly::myRange($n5) && Weekly::myRange($n6)) {
			$sql='INSERT INTO weeklyTickets VALUES (default,'.$n1.','.$n2.','.$n3.','.$n4.','.$n5.','.$n6.');'; 
			$result = $db->Execute($sql);
			if($result) {
				Weekly::fullCheckMatches($db, $ticket);
			}
			else {
				$db = Loader::db(null, null, null, null, true);
				return false;
			}
		}
		else {
			return false;
		}
	
		$db = Loader::db(null, null, null, null, true);
		return $count;
	}
	public static function deleteWeeklyTicket($tId) {

			$ticketId = mysql_real_escape_string($tId);
			//Loader::db();
			$db = Loader::db( 'localhost', 'root', 'root', 'game', true);
			$sql='DELETE FROM weeklyTickets WHERE id='.$ticketId.';'; 
			$result = $db->Execute($sql);
			$db = Loader::db(null, null, null, null, true);
			if($result) {
				return $ticketId;

			} else {
				return false;
			}
	}
	public static function fullCheckMatches() {
		$db = Loader::db( 'localhost', 'root', 'root', 'game', true);
		$weeklies = Weekly::getWeekliesNoId();
		Loader::Model('TicketModel');
		Loader::Model('PlayerModel');
		Loader::Model('CountModel');
		Ticket::resetMatches();
		Count::resetCounts();
		
		$players = Player::getPlayers();
		foreach($players as $player) {
			$tickets = Ticket::getAllTicketsByPid($player['id']);
			if($weeklies && $tickets){
				foreach($weeklies as $weekly)
					foreach ($weekly as $col) {
						$success = false;
						foreach($tickets as $ticket) {
							for($i =1; $i < 7; $i++) {
								if($col == $ticket['n'.$i] && $ticket['c'.$i] != 1) {
									$column = 'c'.$i;
									$tId = $ticket['id'];
									Ticket::matchCol($column, $tId);
						
									$pId = Player::getPlayerIdTicket($tId);
							
									$cId = Count::getCountFromPlayer($pId);
							
									Count::updateCountById($cId);
									//Flush out old tickets to reflect boolean matches
									$tickets = Ticket::getAllTicketsByPid($player['id']);
									$success = true;
								}
								if($success) {
									break;
								}
							}
							if($success) {
								break;
							}
						}
					}
				}
			}
			$db = Loader::db(null, null, null, null, true);
	}
}
?>