<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

class Ticket {
	public static function myRange($value) {
		$min = 1;
		$max = 49;
		if(($min <= $value) && ($value <= $max)) {
			return true;
		}
		else {
			return false;
		}
	}
	public static function getTickets($pId) {
		//Loader::db();
		$db = Loader::db( 'localhost', 'root', 'root', 'game', true);
		$sql='select id, n1, n2, n3, n4, n5, n6 from tickets JOIN ticketOf ON tickets.id = ticketOf.ticket_id WHERE ticketOf.player_id='.$pId.';';
		$result = $db->Execute($sql);
		$db = Loader::db(null, null, null, null, true);
		if(($result) && (!$result->EOF)) {
	 
			while ($row = $result->FetchRow()) {

				$tickets[]= $row;	

			}
		
			return $tickets;
	
		} else {
		
			return false;
	
		}
	}
	public static function getAllTicketsByPid($pId) {
		//Loader::db();
		$db = Loader::db( 'localhost', 'root', 'root', 'game', true);
		$sql='select * from tickets JOIN ticketOf ON tickets.id = ticketOf.ticket_id WHERE ticketOf.player_id='.$pId.';';
		$result = $db->Execute($sql);
		$db = Loader::db(null, null, null, null, true);
		if(($result) && (!$result->EOF)) {
	 
			while ($row = $result->FetchRow()) {

				$tickets[]= $row;	

			}
		
			return $tickets;
	
		} else {
		
			return false;
	
		}
	}
	public static function getAllTickets() {
		//Loader::db();
		$db = Loader::db( 'localhost', 'root', 'root', 'game', true);
		$sql='select * from tickets order by id ASC;';
		$result = $db->Execute($sql);
		$db = Loader::db(null, null, null, null, true);
		if(($result) && (!$result->EOF)) {
	 
			while ($row = $result->FetchRow()) {

				$tickets[]= $row;	

			}
		
			return $tickets;
	
		} else {
		
			return false;
	
		}
	}
	public static function insertTickets($pId, $tickets) {
	
		$playerName = mysql_real_escape_string($pId);
		
		$db = Loader::db( 'localhost', 'root', 'root', 'game', true);
		
		$count = 0;
		foreach ($tickets as $ticket) {
			$n1 = $ticket[0];
			$n2 = $ticket[1];
			$n3 = $ticket[2];
			$n4 = $ticket[3];
			$n5 = $ticket[4];
			$n6 = $ticket[5];
			if(Ticket::myRange($n1) && Ticket::myRange($n2) && Ticket::myRange($n3) && Ticket::myRange($n4) && Ticket::myRange($n5) && Ticket::myRange($n6)) {
				$sql='INSERT INTO tickets VALUES (default,'.$n1.',0,'.$n2.',0,'.$n3.',0,'.$n4.',0,'.$n5.',0,'.$n6.',0);'; 
				$result = $db->Execute($sql);
				if($result) {
					$tId = $db->Insert_ID();
					$sql='INSERT INTO ticketOf VALUES('.$pId.','.$tId.');';
					$result = $db->Execute($sql);
					if($result) {
						$count++;
					}
					else {
						$db = Loader::db(null, null, null, null, true);
						return false;
					}
				}
				else {
					$db = Loader::db(null, null, null, null, true);
					return false;
				}
			}
		}
		$db = Loader::db(null, null, null, null, true);
		return $count;
	}
	public static function deleteTicket($tId) {

			$ticketId = mysql_real_escape_string($tId);
			//Loader::db();
			$db = Loader::db( 'localhost', 'root', 'root', 'game', true);
			$sql='DELETE FROM tickets WHERE id='.$ticketId.';'; 
			$result = $db->Execute($sql);
			$db = Loader::db(null, null, null, null, true);
			if($result) {
				return $ticketId;

			} else {
				return false;
			}

	}
	public static function matchCol($col, $tId) {
		$col = mysql_real_escape_string($col);
		$tId = mysql_real_escape_string($tId);
		$db = Loader::db( 'localhost', 'root', 'root', 'game', true);
		$sql = 'UPDATE tickets SET '.$col.' = 1 WHERE id='.$tId.';'; 
		$result = $db->Execute($sql);
		$db = Loader::db(null, null, null, null, true);
		if($result) {
			return true;

		} else {
			return false;
		}
	}
	public static function resetMatches() {
		$db = Loader::db( 'localhost', 'root', 'root', 'game', true);
		$sql = 'UPDATE tickets SET c1=0, c2=0, c3=0, c4=0, c5=0, c6=0;'; 
		$result = $db->Execute($sql);
		$db = Loader::db(null, null, null, null, true);
		if($result) {
			return true;
		} 
		else {
			return false;
		}
	}
}
?>