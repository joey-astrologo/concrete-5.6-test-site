<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

if ($this->controller->getTask() == 'add') { 
	   if(is_bool($added)) {
		   echo 'Failed To Add Player';
	   }
	   else {
		   echo $added .' was added to the database!';
	   }
}

if ($this->controller->getTask() == 'delete') { 
	   if(is_bool($deleted)) {
		   echo 'Failed To Delete Player';
	   }
	   else {
		   echo $deleted .' was deleted from the database!';
	   }
}

echo '<p>';
echo '<H>Add New Player</H>';	
echo '<form method="post" action="'.$this->action('add').'">
      <input type="text" name="uName" value="" /> 
      <input type="submit" value="Add" />
      </form>';
echo '</p>';	  


if($players) {
	echo '<H>Current List of Players</H>';
	echo '<ul>';

	foreach ($players as $player) {
		echo '<li>'.$player['id'].' - ' .$player['name']. ' - <a href="' . $this->action('delete',$player['id'], $player['name']) .'">Delete</a></li>';
	}

	echo '</ul>';

}
?>	