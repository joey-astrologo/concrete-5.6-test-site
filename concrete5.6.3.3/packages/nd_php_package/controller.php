<?php    

defined('C5_EXECUTE') or die(_("Access Denied."));

class NdPhpPackagePackage extends Package {

	protected $pkgHandle = 'nd_php_package';
	protected $appVersionRequired = '5.3';
	protected $pkgVersion = '1.0';
	
	public function getPackageDescription() {
		return t("Provides a minimalistic PHP block. ");
	}
	
	public function getPackageName() {
		return t("PHP block by ND");
	}
	
	public function install() {
		$pkg = parent::install();
		
		// install block		
		BlockType::installBlockTypeFromPackage('nd_php', $pkg);
	}
}